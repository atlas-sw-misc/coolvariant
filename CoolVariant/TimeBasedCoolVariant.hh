#ifndef _COOL_VARIANT_HH_
#define _COOL_VARIANT_HH_

#include <variant.hpp>
#include <vector>
#include <CoolUtils/TimeBasedCoolWriter.h>
#include <CoolUtils/CoolWritable.h>
#include <CoolUtils/CoolReadable.h>
#include <CoolKernel/IObject.h>
#include <CoolKernel/Record.h>
#include <CoolUtils/TimeBasedCoolReader.h>

class CoolVariantWritable : public daq::coolutils::CoolWritable {
public:
  CoolVariantWritable(const coral::Blob &pt): m_data(pt) {};
protected:
  virtual void fillRecord( cool::Record&) const;
  virtual void setRecordSpec( cool::RecordSpecification& ) const;
private:
  const coral::Blob &m_data;
};

class CoolVariantReadable : public daq::coolutils::CoolReadable { \
public:
  CoolVariantReadable():CoolReadable(){};
  virtual void fetch(coral::Blob &);
};

template <typename V>
class TimeBasedCoolVariantWriter {
public:
    TimeBasedCoolVariantWriter() = delete;
  TimeBasedCoolVariantWriter(const std::string& coolConnString,  const std::string &folder, const std::string &tag):
    m_coolConn(coolConnString),m_folder(folder),m_tag(tag),
        m_writer(daq::coolutils::TimeBasedCoolWriter(m_coolConn,m_folder,m_tag)) {}

void write(const V &v,time_t ts,unsigned channel =0) {
      std::vector<uint8_t> byteBuffer;
      MemoryWriteBuffer writeBuffer(byteBuffer);
      msgpack::Writer msgwriter(writeBuffer, v) ;
      coral::Blob blob(byteBuffer.size());
      memcpy(blob.startingAddress(), byteBuffer.data(),byteBuffer.size());
      CoolVariantWritable obj(blob);
      m_writer.write(ts,obj,channel);
}


private:
  std::string m_coolConn;
  std::string m_folder;
  std::string m_tag;
  daq::coolutils::TimeBasedCoolWriter m_writer;

};
template <typename V>
class TimeBasedCoolVariantReader {
public:
    TimeBasedCoolVariantReader() = delete;
  TimeBasedCoolVariantReader(const std::string& coolConnString,  const std::string &folder, const std::string &tag, bool doubleConv=false):
    m_coolConn(coolConnString),m_folder(folder),m_tag(tag),m_doubleConv(doubleConv),
    m_reader(daq::coolutils::TimeBasedCoolReader(m_coolConn,m_folder,m_tag)) {}

void read_all(V &res,time_t ts) {
    std::vector<daq::coolutils::CoolReadable> objects;
    res=V::array();
    m_reader.read(objects, ts);
    for (auto const &obj:objects ) {
        coral::Blob data;
        if (obj.object() != 0) {
            auto const &record = obj.object()->payload();
            uint32_t channelId = obj.object()->channelId();
            const cool::ValidityKey &validity=obj.object()->since();
            uint32_t timestamp=validity/1000000000;
            data = record["Data"].data<coral::Blob>();
            MemoryReadBuffer buffer((char *) data.startingAddress(), data.size());
            V p;
            msgpack::Reader(buffer, p, m_doubleConv);
            V v;
            v["channelId"]= channelId;
            v["payload"]= p;
            v["timestamp"] =timestamp;
            res.push_back(v);
        }
    }
}

void read(V &v,time_t ts,unsigned count=0) {
    coral::Blob data;
    CoolVariantReadable obj;
    m_reader.read(obj, ts,count);
    obj.fetch(data);
    if(data.size()==0) {
        return;
    }
    MemoryReadBuffer buffer((char*)data.startingAddress(),data.size());
    msgpack::Reader(buffer,v,m_doubleConv);
}

private:
  std::string m_coolConn;
  std::string m_folder;
  std::string m_tag;
  bool m_doubleConv;
  daq::coolutils::TimeBasedCoolReader m_reader;
};

#endif
