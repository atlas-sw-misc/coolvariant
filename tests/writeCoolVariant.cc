#include <iostream>
#include <sstream>
#include <locale>
#include <iomanip>
#include <cmdl/cmdargs.h>
#include "CoolVariant/TimeBasedCoolVariant.hh"
#include <variant.hpp>

int main(int argc,char *argv[])
{
 
  CmdArgStr   timestamp ('u', "timestamp", "timestamp", "timestamp");
  CmdArgStr   dbFolder ('f', "folder", "database-folder", "string",CmdArg::isVALREQ);
  CmdArgStr   dbConn('d',"dbConn","database-connection","string",CmdArg::isVALREQ );
  CmdArgStr   dbTag('t',"dbTag","database-tag","string",CmdArg::isVALREQ);
  CmdArgStr   jsonFile('j',"json","json-file","filename",CmdArg::isVALREQ);


  CmdLine  cmd(*argv, &timestamp,&dbFolder,&dbConn,&dbTag,&jsonFile,NULL);
  CmdArgvIter  arg_iter(--argc, ++argv);
  cmd.set(CmdLine::NO_ABORT|CmdLine::QUIET);
  cmd.parse(arg_iter);
  time_t ts;
  if(timestamp) {
      std::tm t = {};
      std::istringstream ss((const char *) timestamp);
      ss >> std::get_time(&t, "%Y-%m-%d %H:%M:%S");
      if (ss.fail()) {
          std::cerr << "Parsing timestamp failed\n";
          return -1;
      } else {
          std::cout << std::put_time(&t, "%c") << '\n';
      }
      ts=std::mktime(&t);
  } else ts=std::time(NULL);
  std::ifstream jf;
  variant32 v;
  std::cout << "Reading " << (const char *) jsonFile << std::endl;
  try {
      jf.open((const char *) jsonFile);
  } catch(...) {
      std::cerr  << "Can't open JSON file: " << (const char*) jsonFile << std::endl;
      return -1;
  }
   try { variant32::parse(jf,v); } catch (...) {
       std::cerr << "Can't parse JSON file\n";
       return -1;
   }variant32::parse(jf,v);
   v.dump();

  try {
   TimeBasedCoolVariantWriter<variant32> cool((const char*)dbConn,(const char *)dbFolder,(const char*)dbTag);
   cool.write(v,ts);

  } catch(...) {
    std::cerr << "Cool write failed\n"; return -1;
  }
  return 0;
}

