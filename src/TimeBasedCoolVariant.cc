#include <CoolKernel/IObject.h>
#include <CoolKernel/Record.h>
#include <CoolKernel/RecordSpecification.h>

#include "CoolVariant/TimeBasedCoolVariant.hh"


void CoolVariantWritable::fillRecord(cool::Record& record )const {
  record["Data"].setValue<coral::Blob>(m_data);
}

void CoolVariantWritable::setRecordSpec(cool::RecordSpecification& recordSpec  ) const {
  recordSpec.extend("Data", cool::StorageType::Blob16M);
}

void CoolVariantReadable::fetch(coral::Blob &blob) {
  if(object()!=0) {
    const cool::IRecord& record = object()->payload();
    blob=record["Data"].data<coral::Blob>();
  } else blob.resize(0); 
}






